package com.example.chrome.htctest;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public List<Employee> list;
    public final AppCompatActivity appCompatActivity;

    public RecyclerAdapter(List<Employee> list, AppCompatActivity appCompatActivity) {
        this.list = list;
        this.appCompatActivity = appCompatActivity;
    }

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter.ViewHolder holder, int position) {
        holder.employee.setText(list.get(position).name + "\n" + list.get(position).phone + "\n" + list.get(position).skills.toString());

        holder.employee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(appCompatActivity, holder.employee.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView employee;

        public ViewHolder(View itemView) {
            super(itemView);
            employee = itemView.findViewById(R.id.employee);
        }
    }
}





















