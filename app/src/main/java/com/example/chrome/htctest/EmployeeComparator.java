package com.example.chrome.htctest;

import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class EmployeeComparator implements java.util.Comparator<Employee> {


    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getName().compareTo(o2.getName());
    }

    @Override
    public java.util.Comparator<Employee> reversed() {
        return null;
    }

    @Override
    public java.util.Comparator<Employee> thenComparing(java.util.Comparator<? super Employee> other) {
        return null;
    }

    @Override
    public <U> java.util.Comparator<Employee> thenComparing(Function<? super Employee, ? extends U> keyExtractor, java.util.Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> java.util.Comparator<Employee> thenComparing(Function<? super Employee, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public java.util.Comparator<Employee> thenComparingInt(ToIntFunction<? super Employee> keyExtractor) {
        return null;
    }

    @Override
    public java.util.Comparator<Employee> thenComparingLong(ToLongFunction<? super Employee> keyExtractor) {
        return null;
    }

    @Override
    public java.util.Comparator<Employee> thenComparingDouble(ToDoubleFunction<? super Employee> keyExtractor) {
        return null;
    }
}
