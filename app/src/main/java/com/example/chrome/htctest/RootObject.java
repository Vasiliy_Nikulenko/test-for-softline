package com.example.chrome.htctest;

public class RootObject {

    public Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public RootObject(Company company) {
        this.company = company;
    }
}
