package com.example.chrome.htctest;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static RecyclerView recyclerView;
    public static List<Employee> employees;
    public static ParseTask parseTask;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        parseTask = new ParseTask();
        parseTask.execute();
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.employees_rv);
        recyclerView.setLayoutManager((RecyclerView.LayoutManager) new LinearLayoutManager((Context) MainActivity.this));

        RecyclerAdapter adaptor = new RecyclerAdapter(employees, this);
        recyclerView.setAdapter(adaptor);
    }

    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String result = "error";

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://www.mocky.io/v2/56fa31e0110000f920a72134");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                String receivedString = new String();

                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    receivedString = receivedString + line;
                }
                result = receivedString;
            } catch (Throwable throwable) {
                return null;
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("error")) {
                Toast.makeText(context, result, Toast.LENGTH_LONG);
                return;
            }
            employees = new JsonReceiver().JSON(result, RootObject.class).company.employees;
            initRecyclerView();
        }
    }
}


























