package com.example.chrome.htctest;

import com.google.gson.Gson;

public class JsonReceiver {
    public Gson gson = new Gson();

    public RootObject JSON(String s, Class<RootObject> o) {
        RootObject ob = gson.fromJson(s, RootObject.class);
        return ob;
    }

}
