package com.example.chrome.htctest;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Employee implements Comparable<Employee> {

    public String name;
    @SerializedName("phone_number")
    public String phone;
    public List<String> skills;

    public Employee(String name, String phone, List<String> skills) {
        this.name = name;
        this.phone = phone;
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<String> getSkills() {
        return skills;
    }

    @NonNull
    public int compareTo(Employee employee) {
        return getName().compareTo(employee.getName());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", skills=" + skills +
                '}';
    }


}
