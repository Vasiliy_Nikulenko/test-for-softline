package com.example.chrome.htctest;

import java.util.List;

public class Company {

    public String name, age;
    public List<String> competences;
    public List<Employee> employees;


    public Company(String name, String age, List<String> competences, List<Employee> employees) {
        this.name = name;
        this.age = age;
        this.competences = competences;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<String> getCompetences() {
        return competences;
    }

    public void setCompetences(List<String> competences) {
        this.competences = competences;
    }

    public Employee getEmployee(int index) {
        return employees.get(index);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
